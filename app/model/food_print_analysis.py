from datetime import datetime
from .. import mongo
from bson import ObjectId
from ..utils.time import get_month_start
from itertools import groupby
from .food_print import FoodPrint


class FoodPrintAnalysis:
    """
    provide function to manipulate FoodPrint Model and data
    """
    @classmethod
    def _get_collection(cls):
        return mongo.db.analysis

    @classmethod
    def get_food_print(cls, user_id, granularity, start, end):
        """
        get food print of a user or global

        - Currently only support monthly food print
        - The time coverage should cover a start of a month or that month will be excluded
        - If user id is not given, all users' food print is retrieved

        :param user_id:
        :param granularity: accept "monthly" for now
        :param start: start timestamp
        :param end: end timestamp
        :return: array of FoodPrint objects
        """
        col = cls._get_collection()
        query = {
            FoodPrint.GRANULARITY: granularity,
            FoodPrint.START_TIME: {'$gte': datetime.fromtimestamp(start)}
        }

        if user_id:
            query[FoodPrint.USER_ID] = ObjectId(user_id)

        if end > start:
            query[FoodPrint.START_TIME]['$le'] = datetime.fromtimestamp(end)
        res = col.find(query)
        return [FoodPrint(item) for item in res]

    @classmethod
    def fill_monthly_missing_data(cls, start, end, food_print):
        """
        a utility function to fill missing months with zero data

        - Example: A client wants to get monthly food print form March to May of a user,
        but the user just joined this month May. This function will craft and insert data
        for March and May, with data {total:{consumed: 0, wasted: 0, total: 0}}

        :param start: start timestamp
        :param end: end timestamp
        :param food_print: record
        :return: array of FoodPrint object
        """
        s = datetime.fromtimestamp(start)
        e = datetime.utcnow()
        if end > start:
            e = datetime.fromtimestamp(end)

        result = food_print.copy()
        existing_month = [getattr(x, FoodPrint.START_TIME).replace(tzinfo=None) for x in food_print]
        index = 0
        while True:
            month_start = get_month_start(index, e)
            if month_start < s:
                break
            if month_start not in existing_month:
                result.append(FoodPrint({FoodPrint.START_TIME: month_start, FoodPrint.GRANULARITY: 'monthly'}))
            index += 1
        return result

    @classmethod
    def aggregate_food_print_global(cls, food_print):
        """
        aggregate monthly food print of all users as one record

        - Since global food print has multiple entries of different user for a month
        This function will group all entries belong to different user in a month
        and aggregate to one entry

        :param food_print:
        :return: array of FoodPrint object
        """
        result = []
        sorted_fdp = sorted(food_print, key=lambda fdp: getattr(fdp, FoodPrint.START_TIME))
        for dt, group in groupby(sorted_fdp, key=lambda fdp: getattr(fdp, FoodPrint.START_TIME)):
            new_fp = FoodPrint.new_item(uid='', granularity='monthly', fp_type='', start_time=dt, data={})
            for fp in group:
                new_fp += fp
            result.append(new_fp)
        return result
