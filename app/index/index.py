from . import index_blueprint as index


@index.route('/index/ping', methods=['GET'])
def ping():
    return '', 204
