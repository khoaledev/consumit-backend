import re
from werkzeug.security import generate_password_hash, check_password_hash
from werkzeug.exceptions import BadRequest, Unauthorized
from secrets import token_hex


def validate_password(password):
    """
    check for valid password
    check if it is at least 8 characters
    check if it has at least 1 upper case character
    check if it has at least 1 lower case character
    check if it has at least 1 special character [@, #, $, %, ^, &, +, =]
    :param password:
    :return:
    """
    errors = []
    if not re.search(r'.{8,}', password):
        errors.append('must be at least 8 characters')
    if not re.search(r'[A-Z]', password):
        errors.append('must have at least 1 upper case character')
    if not re.search(r'[a-z]', password):
        errors.append('must have at least 1 lower case character')
    if not re.search(r'[@#$%^&+=]', password):
        errors.append('must have at least 1 special character (@ # $ % ^ & + =)')
    if errors:
        raise BadRequest('password ' + ', '.join(errors))


def generate_pass_hash(password):
    """
    hash the password
    :param password:
    :return:
    """
    return generate_password_hash(password)


def check_pass_hash(hashed_pass, password):
    """
    check if the hashed passwords matching
    :param hashed_pass:
    :param password:
    :return:
    """
    if not check_password_hash(hashed_pass, password):
        raise Unauthorized('wrong password')


def generate_random_hex():
    """
    generate a random hex
    :return:
    """
    return token_hex()
