"""
Mongo model is the base model that can be inherited by other entity like user or food item
"""
import inspect
# from copy import copy
from bson import ObjectId
from ..exceptions import ResourceNotFound
import copy


class MongoModel(object):
    """
    parent class support common methods for all child Models.
    support utility functions:
        find: find by id.
        to_dict: convert model object to dictionary.
        from_dict: load data from document/dictionary to Model object.
    """
    MG_ID = '_id'

    def __init__(self, iterable):
        """
        parse dict to MongoModel
        parse the MongoModel or child to MongoModel or child

        raise exception if iterable is neither of a dictionary of MongoModel

        :param iterable: a dictionary represent mongodb document or MongoModel child
        """
        if isinstance(iterable, type(self)):
            self.from_dict(iterable.__dict__.copy())
        elif isinstance(iterable, dict):
            self.from_dict(iterable)
        else:
            raise Exception('cannot cast from {} to {}.'.format(type(iterable), type(self)))

    @classmethod
    def _get_model_keys(cls):
        if not hasattr(cls, 'model_keys'):
            attributes = inspect.getmembers(cls, lambda a: not inspect.isroutine(a))
            cls.model_keys = [a[1] for a in attributes if not (a[0].startswith('__') and a[0].endswith('__'))]
        return cls.model_keys

    @classmethod
    def find(cls, mg_id):
        """
        Find a doc by doc id
        :param mg_id: document mongo id, string or ObjectId
        :return: Mongo Model object
        """
        res = cls._get_collection().find_one({cls.MG_ID: ObjectId(mg_id)})
        if not res:
            raise ResourceNotFound(resource=cls.__name__, message=mg_id)
        return cls(res)

    @classmethod
    def _get_collection(cls):
        raise NotImplementedError

    def from_dict(self, doc, include=[], exclude=[]):
        """
        load data from document/dictionary to Model object.
        Only class pre-defined keys will be parsed.
        All other fields in doc will be ignored.
        :param doc:     A dictionary represent mongodb document
        :param include: An array for partial parsing.
                        Only fields in this array is parsed
        :param exclude: An array for partial parsing.
                        Fields in this array will NOT be parsed
        :return:        Model object
        """
        keys = [x for x in include if x not in exclude]
        if not keys:
            keys = self.__class__._get_model_keys()
        for key in keys:
            if key in doc:
                setattr(self, key, doc.get(key))

    def to_dict(self, include=(), exclude=(), deepcopy=False):
        """
        convert model object to dictionary.
        :param include: An array for partial converting.
                        Only fields in this array is converted.
        :param exclude: An array for partial converting.
                        Fields in this array will NOT be converted.
        :param deepcopy: to make a deepcopy of object or shallow copy
                        Most of the time, shallow copy is fine
                        However, if the returned dictionary is going to be modified,
                        it's better to have a deepcopy

        :return:        A dictionary contains object data.
        """
        # deep copy to avoid deletion and replacement
        if deepcopy:
            d = copy.deepcopy(self.__dict__)
        else:
            d = self.__dict__.copy()

        if include:
            d = {k: d[k] for k in d if k in include}
        if exclude:
            d = {k: d[k] for k in d if k not in exclude}
        return d

    def __iter__(self):
        d = self.__dict__
        for key, value in d.items():
            yield key, value

    def __str__(self):
        return str(dict(self))

    def __repr__(self):
        return dict(self)
