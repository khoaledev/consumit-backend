"""
food item model
"""
from datetime import datetime, timedelta
from flask import abort
from .mongo_model import MongoModel
from .. import mongo
from bson import ObjectId
from werkzeug.exceptions import BadRequest
from ..utils.time import get_day_start


class FoodItem(MongoModel):
    """
    representing food item in food item mongo collection
    """
    EXPIRY_EXPIRED = 'expired'
    EXPIRY_CONSUMED = 'consumed'
    EXPIRY_BOTH = 'both'

    USER_ID = 'user_id'
    NAME = 'name'
    TYPE = 'type'
    QUANTITY = 'quantity'
    QNT_TYPE = 'quantity_type'
    INPUT_DATE = 'input_date'
    EXP_DATE = 'exp_date'
    EXP_DAYS = 'exp_days'
    IS_EXPIRED = 'is_expired'
    IS_CONSUMED = 'is_consumed'
    CONSUMPTION = 'consumption'
    LAST_UPDATE = 'last_update'
    DAYS_LEFT = 'days_left'
    HOURS_LEFT = 'hours_left'

    _INIT_FIELDS = (NAME, TYPE, EXP_DAYS, QUANTITY, QNT_TYPE)

    def __init__(self, iterable):
        """
        caste food item dictionary to Food Item

        compute time left in days, if days == 0, compute hours left
        check if item is expired and save to DB

        :param iterable:
        """
        super().__init__(iterable)
        day_start = get_day_start()
        time_left = getattr(self, FoodItem.EXP_DATE).replace(tzinfo=None) - day_start
        days_left = time_left.days
        setattr(self, FoodItem.DAYS_LEFT, days_left)
        hour_left = (24 - datetime.utcnow().hour) if days_left > 0 else (- datetime.utcnow().hour)
        setattr(self, FoodItem.HOURS_LEFT, hour_left)
        if self._consumed() or self._wasted():
            return
        if days_left < 0:
            self._wasted(True)
            self.save(FoodItem.IS_EXPIRED)

    @classmethod
    def new_item(cls, uid, name, food_type, quantity, qnt_type, exp_days, message):
        """
        creating new food item belongs to a user
        :param uid: user id
        :param name: food name
        :param food_type: food type
        :param quantity: quantity in float
        :param qnt_type: quantity type, count or pound
        :param exp_days: number of expiration days
        :param message: optional message
        :return: Food Item object
        """
        js = cls._new_item_json(uid, name, food_type, quantity,
                                qnt_type, exp_days, message)
        return FoodItem(js)

    def validate_init_data(self):
        """
        validate initial data
        check if data is missing
        check if quantity is positive
        check if expiration days is greater than or equal to zero
        check if quantity type is ['lb', 'count', 'oz', 'fl oz', 'gal']
        :return:
        """
        missing = [key for key in self.__class__._INIT_FIELDS if getattr(self, key) == '' or getattr(self, key) is None]
        if missing:
            raise BadRequest('missing fields: {}'.format(missing))
        if getattr(self, FoodItem.QUANTITY) <= 0:
            raise BadRequest('quantity has to be greater than 0')
        if getattr(self, FoodItem.EXP_DAYS) < 0:
            raise BadRequest('expiration days cannot be negative')
        if getattr(self, FoodItem.QNT_TYPE) not in ['lb', 'count', 'oz', 'fl oz', 'gal']:
            raise BadRequest('invalid quantity type {}. Must be one of {}'
                             .format(getattr(self, FoodItem.QNT_TYPE), ['lb', 'count', 'oz', 'fl oz', 'gal']))

        return self

    @classmethod
    def _new_item_json(cls, uid, name, food_type, quantity, qnt_type, exp_days, message):
        """
        create json representing food item
        :param uid:
        :param name:
        :param food_type:
        :param quantity:
        :param qnt_type:
        :param exp_days:
        :param message:
        :return:
        """
        utcnow = datetime.utcnow()
        item = {
            cls.USER_ID: ObjectId(uid),
            cls.NAME: name.lower(),
            cls.TYPE: food_type.lower(),
            cls.QUANTITY: quantity,
            cls.QNT_TYPE: qnt_type,
            cls.INPUT_DATE: utcnow,
            cls.EXP_DAYS: exp_days,
            cls.EXP_DATE: utcnow + timedelta(days=exp_days),
            cls.IS_CONSUMED: False,
            cls.IS_EXPIRED: False,
            cls.LAST_UPDATE: utcnow,
            cls.CONSUMPTION: [
                cls._build_consumption_entry(action='input', amount=quantity,
                                             quantity=quantity, message=message, time=utcnow)
            ]
        }
        return item

    def create(self):
        """
        insert ONE item (self) into db
        item should be validated before inserted
        :return: self
        """
        doc = self.to_dict()
        mongo.db.food_items.insert_one(doc)
        setattr(self, FoodItem.MG_ID, doc.get(FoodItem.MG_ID))
        return self

    def validate_and_create(self):
        """
        validate and insert ONE item into db
        :return: FoodItem self
        """
        self.validate_init_data()
        self.create()
        return self

    def update_quantity(self, amount, action, message=None):
        """
        update food item quantity and consumption record
        validate data before updating
            check if action is one of ['eat']
            check if amount > 0
            check if action amount < current quantity
        compute new quantity, build and insert new consumption record
        check consumed or expired
        update lastupdate field and save

        :param amount: action related quantity
        :param action: action name
        :param message: optional message
        :return:
        """

        # compute the new quantity
        if action not in ['eat']:
            raise BadRequest('action {} must be one of {}'. format(action, ['eat']))
        if amount <= 0:
            raise BadRequest('amount must be greater than 0')
        new_qunt = getattr(self, FoodItem.QUANTITY) - amount
        if new_qunt < 0:
            raise BadRequest('action amount cannot be greater than current quantity')

        # build record entry of consumption
        utcnow = datetime.utcnow()
        entry = self._build_consumption_entry(action, amount, new_qunt, message, utcnow)
        self._update_consumption_record(entry)

        # update
        setattr(self, FoodItem.QUANTITY, new_qunt)
        self._check_wasted_or_consumed(new_qunt, action)
        setattr(self, FoodItem.LAST_UPDATE, utcnow)
        self.save(FoodItem.QUANTITY, FoodItem.IS_EXPIRED, FoodItem.IS_CONSUMED, FoodItem.CONSUMPTION,
                  FoodItem.LAST_UPDATE)

    def _check_wasted_or_consumed(self, quantity, action):
        """
        if consumed or wasted, set the corresponding attribute
        :param quantity:
        :param action:
        :return:
        """
        if quantity == 0:
            if action == 'eat':
                self._consumed(True)
            elif action == 'throw':
                self._wasted(True)

    def _consumed(self, value=None):
        """
        set or read is_consumed attribute
        :param value:
        :return:
        """
        if value is not None:
            setattr(self, FoodItem.IS_CONSUMED, value)
            return value
        else:
            return getattr(self, FoodItem.IS_CONSUMED, False)

    def _wasted(self, value=None):
        """
        set or read is_expired attribute
        :param value:
        :return:
        """
        if value is not None:
            setattr(self, FoodItem.IS_EXPIRED, value)
            return value
        else:
            return getattr(self, FoodItem.IS_EXPIRED, False)

    def _update_consumption_record(self, record_entry):
        """
        insert a new consumption record into consumption
        :param record_entry:
        :return:
        """
        consumption = getattr(self, FoodItem.CONSUMPTION)
        consumption.append(record_entry)
        setattr(self, FoodItem.CONSUMPTION, consumption)

    @classmethod
    def _build_consumption_entry(cls, action, amount, quantity, message, time=datetime.utcnow()):
        return {'action': action,
                'amount': amount,
                'quantity': quantity,
                'message': message,
                'timestamp': time,
                'time': time}

    def save(self, *fields):
        """
        update the item, if item is not created (not having item id), not saving
        :param fields: fields to save
        :return:
        """
        iid = getattr(self, FoodItem.MG_ID, None)
        if not iid:
            return
        doc = self.to_dict(include=fields, exclude=[FoodItem.MG_ID])
        mongo.db.food_items.update_one({FoodItem.MG_ID: ObjectId(iid)},
                                       {'$set': doc})

    @classmethod
    def _get_collection(cls):
        """
        get the food item collection
        :return: food items collection
        """
        return mongo.db.food_items

    @classmethod
    def find_by_user_id(cls, uid, start, end, page=0, size=0,
                        expiry=EXPIRY_BOTH, name='', food_type=''):
        """
        find food item by user id and input date within start and end

        support pagination
        support filter by name or food type
        sorted in descending order by input date

        :param uid: user id
        :param start: start timestamp
        :param end: end timestamp
        :param page: page number
        :param size: page size
        :param expiry: expired | consumed | both
        :param name: name of food item for filtering
        :param food_type: type of food item for filtering
        :return: list of Food items sorted in descending order by input date
        """
        match = {FoodItem.USER_ID: ObjectId(uid)}
        match = cls._build_start_end_match_query(match, FoodItem.INPUT_DATE, start, end)
        match = cls._build_expiry_match_query(match, expiry)

        # filter by either name or food type
        if name:
            match = cls._build_filtering_query(match, FoodItem.NAME, name)
        elif food_type:
            match = cls._build_filtering_query(match, FoodItem.TYPE, food_type)

        # sort in descending order by input date
        sort = {FoodItem.INPUT_DATE: -1}
        pipline = [{'$match': match}, {'$sort': sort}]

        # if page size is given, enable pagination
        if size > 0:
            pipline.extend([{'$skip': page * size}, {'$limit': size}])
        items = mongo.db.food_items.aggregate(pipline)
        return [FoodItem(item) for item in items]

    @classmethod
    def _build_start_end_match_query(cls, init_query, match_field, start, end):
        """
        build matching MONGO query so that the match field has value less then end and greater than start

        if start == 0, start = start of today in utc
        if end == 0, end is to infinity

        :param init_query: previous match query, current query will be added to it
        :param match_field: the key that users wish to put into the range
        :param start: timestamp
        :param end: timestamp
        :return: matching query dictionary
        """
        match = init_query or {}
        exp_match = {'$gte': get_day_start()}
        if start != 0:
            exp_match = {'$gte': datetime.fromtimestamp(start)}
        if end > start:
            exp_match['$lt'] = datetime.fromtimestamp(end)
        match[match_field] = exp_match
        return match

    @classmethod
    def _build_expiry_match_query(cls, init_match, expiry):
        """
        append expiry matching to init_match dictionary

        if expired == 'consumed', check if items are consumed
        else if expired == 'expired', check if item are not consumed
        else treat as 'both', not checking

        ATTENTION: 'expired' is not 'expired', but would rather 'not consumed'
        to get true expired food items, query item that has 'expired' expiry
        and expiration date in the past

        IMPROVEMENT: now food item will automatically check and set is_expired field
        if an item is expired

        :param init_match:
        :param expiry: expiry value
        :return: match query
        """
        match = init_match or {}
        if expiry == cls.EXPIRY_EXPIRED:
            match[FoodItem.IS_CONSUMED] = False
        elif expiry == cls.EXPIRY_CONSUMED:
            match[FoodItem.IS_CONSUMED] = True
        return match

    @classmethod
    def _build_filtering_query(cls, init_match, filtered_field, filtered_value):
        """
        append filtering query to init matching
        :param init_match:
        :param filtered_field:
        :param filtered_value:
        :return: match query
        """
        match = init_match or {}
        if filtered_field:
            match[filtered_field] = filtered_value
        return match

    @classmethod
    def find_item_expiry(cls, uid, start, end, page=0, size=0,
                         expiry=EXPIRY_BOTH, name='', food_type=''):
        """
        find all food items of a user which have expiration stay within start and end date

        support pagination
        support filter by name or food type
        sorted in ascending order by expiration date

        :param uid: user id
        :param start: start timestamp
        :param end: end timestamp
        :param page: page number
        :param size: page size
        :param expiry: expired | consumed | both
        :param name: name of food item for filtering
        :param food_type: type of food item for filtering
        :return: list of Food items sorted in ascending order by expiration date
        """
        match = {FoodItem.USER_ID: ObjectId(uid)}
        match = cls._build_start_end_match_query(match, FoodItem.EXP_DATE, start, end)
        match = cls._build_expiry_match_query(match, expiry)

        # filter by name OR food type
        if name:
            match = cls._build_filtering_query(match, FoodItem.NAME, name)
        elif food_type:
            match = cls._build_filtering_query(match, FoodItem.TYPE, food_type)

        # sort in ascending order by expiration date
        sort = {FoodItem.EXP_DATE: 1}
        pipline = [{'$match': match}, {'$sort': sort}]

        # if page size is given, enable pagination
        if size > 0:
            pipline.extend([{'$skip': page * size}, {'$limit': size}])
        items = mongo.db.food_items.aggregate(pipline)
        return [FoodItem(item) for item in items]

    @classmethod
    def remove(cls, iid):
        """
        delete a food item
        :param iid: item id
        :return: True/False
        """
        return mongo.db.food_items.delete_one({FoodItem.MG_ID: ObjectId(iid)})
