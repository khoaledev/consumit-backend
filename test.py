class A:
    def __init__(self, num):
        self.num = num

    def __add__(self, other):
        if not isinstance(other, A):
            raise Exception('AAA')
        return A(self.num + other.num)


a = A(1)
b = A(1)

c = a + b
a = a + b

a + 1
print(a.num)
print(b.num)
print(c.num)