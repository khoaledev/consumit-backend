"""
Config
"""
import os


class Config:
    """
    place to define common Flask app configuration
    """
    SECRET_KEY = os.environ.get('CONSUMIT_KEY') or "OnlyIfYouCanGuess"

    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):
    """
    place to define development environment configuration.
    """
    DEBUG = True

    # MONGO_HOST = 'localhost'
    # MONGO_PORT = 27017
    # MONGO_DBNAME = "consumit"

    MONGO_HOST = 'ds259085.mlab.com'
    MONGO_PORT = 59085
    MONGO_DBNAME = 'consumitdev'
    MONGO_USERNAME = 'consumitDev'
    MONGO_PASSWORD = 'consumitNov2017#'

    AWS_SQS_SECRET_KEY = 'k0DLRHZ81r9PwOXtdO0O7fxUrwysnuR63o9aTD0V'
    AWS_SQS_ACCESS_ID = 'AKIAJYQI523TE75EP75A'
    AWS_SQS_QUEUE_NAME = 'consumit_analytics'
    AWS_SQS_QUEUE_URL = 'https://sqs.us-east-1.amazonaws.com/194862814775/consumit_analytics'
    AWS_SQS_REGION_NAME = 'us-west-1'

    AWS_PINPOINT_SECRET_KEY = 'g2JedxZRUrAbf3EzghTwC73Nrn5jH/JRbnCo9csa'
    AWS_PINPOINT_ACCESS_ID = 'AKIAJPNOZFK5Q5OZDNQQ'
    AWS_PINPOINT_REGION_NAME = 'us-east-1'
    AWS_PINPOINT_APPLICATION_ID = '886f6f0e5e384b86991525e0ffec6cf8'


class TestConfig(Config):
    """
    place to define testing environment configuration. This is used in unit-test.
    """
    TESTING = True

    MONGO_HOST = 'localhost'
    MONGO_PORT = 27017
    MONGO_DBNAME = 'consumittest'

    AWS_SQS_SECRET_KEY = 'k0DLRHZ81r9PwOXtdO0O7fxUrwysnuR63o9aTD0V'
    AWS_SQS_ACCESS_ID = 'AKIAJYQI523TE75EP75A'
    AWS_SQS_QUEUE_NAME = 'consumit_analytics'
    # AWS_SQS_QUEUE_URL = 'https://sqs.us-east-1.amazonaws.com/194862814775/consumit_analytics'
    AWS_SQS_REGION_NAME = 'us-west-1'

    # MONGO_HOST = 'ds259085.mlab.com'
    # MONGO_PORT = 59085
    # MONGO_DBNAME = 'consumittest'
    # MONGO_USERNAME = 'consumitTest'
    # MONGO_PASSWORD = 'consumitNov2017#'
    # MONGO_AUTH_MECHANISM = 'SCRAM-SHA-1'


class PreProductionConfig(Config):
    """
    place to define production environment configuration.
    """
    # MONGO_DBNAME = "consumit"
    # MONGO_USERNAME = 'consumit-dev'
    # MONGO_PASSWORD = 'djn05z8g6dYF7IEA'
    # MONGO_REPLICA_SET = 'Cluster0-shard-0'
    # MONGO_AUTH_MECHANISM
    pass

class ProductionConfig(Config):
    """
    place to define production environment configuration.
    """
    # MONGO_DBNAME = ""
    # MONGO_USERNAME
    # MONGO_PASSWORD
    # MONGO_AUTH_MECHANISM
    pass


CONFIG = {
    'development': DevelopmentConfig,
    'testing': TestConfig,
    'pre-production': PreProductionConfig,
    'production': ProductionConfig,

    'default': DevelopmentConfig
}

class FoodItemConfig():
    DEFAULT_PAGE = 0
    DEFAULT_PAGE_SIZE = 10
