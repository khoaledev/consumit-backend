"""
Users controllers
"""
import logging
from flask import request, jsonify
from . import api_1_0_blueprint as api
from ..utils import auth_utils
from ..model.user import User
from .. import sqs
from ..model.push_notification import PushNotification as Push
from datetime import datetime


logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')


@api.route('/users/signup', methods=['POST'])
def signup():
    """
    signup api
    also perform sign in afterward
    :return: user token, 201
    """
    r_json = request.get_json()
    email = r_json.get('email')
    pwd = r_json.get('password')
    user = User.new_user(email, pwd)\
        .signup()\
        .signin()
    return jsonify(user.get_token()), 201


@api.route('/users/signin', methods=['POST'])
def signin():
    """
    signin api
    :return: user token, 200
    """
    r_json = request.get_json()
    email = r_json.get('email')
    pwd = r_json.get('password')
    user = User.find_by_email(email)\
        .verify_pwd(pwd)\
        .signin()
    return jsonify(user.get_token())


@api.route('/users/signout', methods=['POST'])
@auth_utils.verify_user_token()
def signout():
    """
    signout api
    if push notification token is given, remove the token.
    :return: 204
    """
    uid = request.headers.get('user_id')
    User.find(uid).signout()
    req_json = request.get_json(silent=True)
    if req_json:
        push_token = req_json.get('push_token', {})
        if push_token:
            User.validate_push_token(push_token.get('token', ''), push_token.get('os', '')).\
                remove_push_token(uid, push_token['token'], push_token['os'])
    return '', 204


@api.route('/users/<user_id>', methods=['GET'])
@auth_utils.verify_user_token()
def get_profile(user_id):
    """
    get user profile API
    :param user_id: currently not used
    :return: user profile, 200
    """
    uid = request.headers.get('user_id')
    user = User.find(uid)
    return jsonify(user.get_profile()), 200


@api.route('/users/<user_id>', methods=['PUT'])
@auth_utils.verify_user_token()
def update_profile(user_id):
    """
    update profile API
    :param user_id: currently not used
    :return: user profile, 200
    """
    uid = request.headers.get('user_id')
    profile = request.get_json()
    user = User.find(uid).update_profile(profile)
    return jsonify(user.get_profile())


@api.route('/users/alexa/<alexa_id>', methods=['GET'])
def get_user_by_alexa_id(alexa_id):
    """
    log a user in given alexa id
    :param alexa_id:
    :return: user authentication tokens
    """
    user = User.find_by_alexa_id(alexa_id)
    return jsonify(user.get_token())


@api.route('/users/<user_id>/alexa/code', methods=['GET'])
@auth_utils.verify_user_token()
def get_alexa_code(user_id):
    """
    generate alexa code to register an alexa device
    :param user_id: currently not used
    :return: alexa code object with code and expiration time
    """
    uid = request.headers.get('user_id')
    user = User.find(uid).generate_alexa_code()
    return jsonify(user.to_dict(include=[User.ALEXA_CODE], exclude=[User.ANALYSIS]))


@api.route('/users/alexa/registration', methods=['PUT'])
def register_alexa():
    """
    register an alexa device. The request should be fired from alexa device
    :return: user authentication token
    """
    # TODO: add authen between alexa and server
    r_json = request.get_json()
    alexa_code = int(r_json.get('alexa_code'))
    alexa_id = r_json.get('alexa_id')
    user = User.register_alexa(alexa_code, alexa_id)
    return jsonify(user.get_token()), 200


@api.route('/users/<user_id>/alexa/remove', methods=['PUT'])
@auth_utils.verify_user_token()
def remove_alexa(user_id):
    """
    remove current alexa id
    :param user_id:
    :return: 204
    """
    uid = request.headers.get('user_id')
    User.remove_alexa(uid)
    return '', 204


@api.route('/users/<user_id>/push-tokens', methods=['PUT'])
@auth_utils.verify_user_token()
def register_push_token(user_id):
    """
    register a user push notification token
    :param user_id:
    :return: user push notification tokens 200
    """
    uid = request.headers.get('user_id')
    r_json = request.get_json()
    token = r_json.get('token')
    os = r_json.get('os')
    res = User.validate_push_token(token, os).\
        register_push_token(uid, token, os)
    return jsonify(res), 200


@api.route('/users/<user_id>/push-tokens', methods=['DELETE'])
@auth_utils.verify_user_token()
def remove_push_token(user_id):
    """
    remove a user push notification token
    :param user_id:
    :return: 204
    """
    uid = request.headers.get('user_id')
    r_json = request.get_json()
    token = r_json.get('token')
    os = r_json.get('os')
    User.validate_push_token(token, os).\
        remove_push_token(uid, token, os)
    return '', 204


@api.route('/users/push-notification/<user_id>', methods=['PUT'])
@auth_utils.verify_secret_handshake()
def send_push_notification(user_id):
    """
    For TESTING purposes

    send expiration push notification to a user or all user
    :param user_id: user_id or "all"
    :return: 204
    """
    if user_id == 'all':
        Push.send_food_expiration()
    else:
        Push.send_food_expiration(user_id)
    return '', 204


@api.route('/users/hourly_batch_process', methods=['PUT'])
@auth_utils.verify_secret_handshake()
def hourly_batch_process():
    """
    decide what to do with all users based on the hour of the day
    Using only UTC as of now
    if hour == 0: run all analysis
    if hour == 0: send expiration push notification
    :return: time and array of processes that has been run, 200
    """
    utcnow = datetime.utcnow()
    batch_process =[]
    if utcnow.hour == 0:
        user_ids = User.get_all_userid()
        sqs.send_update_analysis_messages(user_ids)
        batch_process.append('send update analysis message')
    elif utcnow.hour == 8:
        Push.send_food_expiration()
        batch_process.append('send push notification')
    return jsonify({'time': utcnow, 'processes': batch_process}), 200
