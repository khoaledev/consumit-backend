"""
Users controllers
"""
import logging
from flask import request, jsonify
from . import api_1_0_blueprint as api
from ..utils import auth_utils
from ..model.user import User
from .. import sqs
from ..model.food_print_analysis import FoodPrintAnalysis


logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')


@api.route('/analysis/most_wasted_consumed', methods=['GET'])
@auth_utils.verify_user_token()
def get_most_wasted_consumed_analysis():
    """
    most wasted and consumed analysis
    :return: most wasted consumed analysis, 200
    """
    uid = request.headers.get('user_id', '')
    user = User.find(uid)
    return jsonify(user.get_analysis()), 200


@api.route('/analysis/food_print', methods=['GET'])
@auth_utils.verify_user_token()
def get_food_print():
    """
    food print aggregation
    :return: food print, 200
    """
    uid = request.headers.get('user_id', '')
    start = float(request.args.get('start', 0))
    end = float(request.args.get('end', 0))
    granularity = request.args.get('granularity', 'monthly')
    scope = request.args.get('scope', 'user')
    if scope not in ['global', 'user', 'user_all']:
        scope = 'user'

    if scope == 'user_all':
        user = User.find(uid)
        return jsonify(user.get_analysis()), 200
    elif scope == 'user':
        food_print = FoodPrintAnalysis.get_food_print(user_id=uid, granularity=granularity, start=start, end=end)
        food_print = FoodPrintAnalysis.fill_monthly_missing_data(start, end, food_print)
        return jsonify(food_print), 200
    elif scope == 'global':
        food_print = FoodPrintAnalysis.get_food_print(user_id=None, granularity=granularity, start=start, end=end)
        food_print = FoodPrintAnalysis.aggregate_food_print_global(food_print)
        food_print = FoodPrintAnalysis.fill_monthly_missing_data(start, end, food_print)
        return jsonify(food_print), 200


@api.route('/analysis/<user_id>', methods=['PUT'])
@auth_utils.verify_secret_handshake()
def send_update_analysis_message(user_id):
    """
    send message to update analysis
    only support user_id == all right now.
    :return: 204
    """
    if user_id != 'all':
        return 'only support "all"', 404

    # only support /all right now
    if user_id == 'all':
        user_ids = User.get_all_userid()
        sqs.send_update_analysis_messages(user_ids)
    return '', 204


