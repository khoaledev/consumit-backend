import datetime


class ResourceNotFound(Exception):
    def __init__(self, resource, message, payload=None):
        Exception.__init__(self)
        self.resource = resource
        self.message = message
        self.status_code = 404
        self.payload = payload

    def to_dict(self):
        rv = {}
        rv['payload'] = dict(self.payload or {})
        rv['message'] = self.message
        rv['resource'] = self.resource
        rv['err_type'] = self.__class__.__name__
        rv['time'] = datetime.datetime.now()
        return rv


class InvalidAuthentication(Exception):
    def __init__(self, username='', userid='', message='', payload=None):
        Exception.__init__(self)
        self.username = username
        self.userid = userid
        self.message = message
        self.status_code = 401
        self.payload = payload

    def to_dict(self):
        rv = {}
        rv['payload'] = dict(self.payload or {})
        rv['message'] = self.message
        rv['userid'] = self.userid
        rv['username'] = self.username
        rv['err_type'] = self.__class__.__name__
        rv['time'] = datetime.datetime.now()
        return rv
