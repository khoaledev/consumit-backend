import logging
from flask import request, jsonify
from . import api_1_0_blueprint as api
from ..model.user import User


logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')


@api.route('/recommendation/shopping-list', methods=['GET'])
@api.route('/recommendation/suggestive-shopping-list', methods=['GET'])
def get_user_shopping_list():
    """
    get shopping list api
    :return: shopping list
    """
    uid = request.headers.get('user_id', '')
    user = User.find(uid)

    return jsonify(user.get_suggestive_shopping_list()), 200

