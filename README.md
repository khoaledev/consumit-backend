# Consumit


## running locally

run the following command to start the server, the address is localhost:5000
        python manage.py runserver

run the following command to run the test
        python manage.py test

run the following command to start a shell with basic setup like connection to db
        python manage.py shell


## running in docker

run the following command to build the image
        docker build -t consumit .

run the following command to start the container with interactive shell, the addess is localhost:5000
        docker run -it --rm --name consumit -p 5000:5000 consumit

run the following command to start the container, the address is localhost:5000
        docker run -d --name consumit -p 5000:5000 consumit

## deploying instruction (manually)

build the image
        docker build -t consumit .

login into docker
        docker login

tag the image before pushing
        docker tag consumit azhad/consumit:0.1

push the image to zahad/consumit repository
        docker push azhad/consumit:0.1

go to server, login and pull the image
        docker pull azhad/consumit:0.1

run the server
        docker run -d --name consumit -p 5000:5000 consumit