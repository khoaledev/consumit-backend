import boto3
from datetime import datetime

ADDED_FOOD_MESSAGE = 'added_food'
CONSUMED_FOOD_MESSAGE = 'consumed_food'


class ConsumitSQS(object):
    """
    public interface to send messages to Amazon SQS
    """
    def __init__(self, app=None):
        self.config = {}
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        """
        save all SQS configuration to instance
        :param app: flask app
        :return:
        """
        kwargs = {
            'resource': 'sqs',
            'secret_key': app.config['AWS_SQS_SECRET_KEY'],
            'access_id': app.config['AWS_SQS_ACCESS_ID'],
            'queue_url': app.config.get('AWS_SQS_QUEUE_URL', ''),
            'region_name': app.config['AWS_SQS_REGION_NAME']
        }
        self.config = kwargs

    def send_update_analysis_message(self, user_id, message='food_analysis'):
        """
        send an update analysis to a user given his user id
        :param user_id: user id
        :param message: message to be send
        :return:
        """
        # get the queue
        cf = self.config
        if not cf['queue_url']:
            return
        sqs = boto3.client('sqs',
                           aws_access_key_id=cf['access_id'],
                           aws_secret_access_key=cf['secret_key'],
                           region_name=cf['region_name'])

        # send message with 0 delay
        # inject user_id in message attribute so that the consumer know which user to perform analysis
        response = sqs.send_message(
            QueueUrl=cf['queue_url'],
            MessageBody=message,
            DelaySeconds=0,
            MessageAttributes={
                'user_id': {
                    'StringValue': user_id,
                    'DataType': 'String'
                }}
        )

        if response['ResponseMetadata']['HTTPStatusCode'] != 200:
            # TODO: retry or log the failure
            pass

    def send_update_analysis_messages(self, user_ids, message='food_analysis'):
        """
        send multiple update food analysis messages to multiple users given a list of user ids
        :param user_ids: a list of user id
        :param message: message to be send
        :return:
        """
        batches = ConsumitSQS.build_message_batches(user_ids, message)
        for batch in batches:
            self.send_message_batch(batch)

    def send_message_batch(self, entries):
        """
        helper function to send a batch of messages

        build message batches with 10 messages in each batch
        send the batch and continue

        :param entries:
        :return:
        """
        cf = self.config
        if not cf['queue_url']:
            return
        if entries:
            sqs = boto3.client('sqs',
                               aws_access_key_id=cf['access_id'],
                               aws_secret_access_key=cf['secret_key'],
                               region_name=cf['region_name'])
            sqs.send_message_batch(
                QueueUrl=cf['queue_url'],
                Entries=entries
            )

    @staticmethod
    def build_message_batches(user_ids, message):
        """
        generate batches of messages

        comply with AWS SQS, a batch is limited to 10 messages
        build each message entry with an index

        :param user_ids:
        :param message:
        :return:
        """
        i, entries = 0, []
        for user_id in user_ids:
            entries.append(ConsumitSQS.build_message_entry(index=i, uid=user_id, message=message))
            i += 1
            if i == 10:
                yield entries
                i, entries = 0, []
        yield entries

    @staticmethod
    def build_message_entry(index, uid, message):
        """
        build message entry with index
        comply with AWS SQS each entry need an index

        :param index:
        :param uid: user id
        :param message:
        :return:
        """
        return {
            'Id': str(index),
            'MessageBody': message,
            'DelaySeconds': 0,
            'MessageAttributes': {
                'user_id': {
                    'StringValue': uid,
                    'DataType': 'String'
                }
            }
        }

    def send_added_consumed_food(self, user_id, food_name, quantity, message, food_type, timestamp=datetime.utcnow()):
        cf = self.config
        if not cf['queue_url']:
            return
        sqs = boto3.client('sqs',
                           aws_access_key_id=cf['access_id'],
                           aws_secret_access_key=cf['secret_key'],
                           region_name=cf['region_name'])

        # send message with 0 delay
        # inject user_id in message attribute so that the consumer know which user to perform analysis
        response = sqs.send_message(
            QueueUrl=cf['queue_url'],
            MessageBody=message,
            DelaySeconds=0,
            MessageAttributes={
                'user_id': {
                    'StringValue': str(user_id),
                    'DataType': 'String'
                },
                'food_name': {
                    'StringValue': str(food_name),
                    'DataType': 'String'
                },
                'quantity': {
                    'StringValue': str(quantity),
                    'DataType': 'Number'
                },
                'food_type': {
                    'StringValue': str(food_type),
                    'DataType': 'String'
                },
                'update_timestamp': {
                    'StringValue': str(timestamp),
                    'DataType': 'Number'
                }
            }
        )

        if response['ResponseMetadata']['HTTPStatusCode'] != 200:
            # TODO: retry or log the failure
            pass
