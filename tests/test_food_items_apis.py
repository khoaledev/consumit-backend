import unittest
from flask import current_app, url_for, json, Response
from app import create_app, mongo
from testing import ConSumitTestClient
from datetime import datetime, timedelta


class FoodItemsTestCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        self.collection = mongo.db.food_items
        self.collection.remove()
        mongo.db.users.remove()
        self.client = ConSumitTestClient(self.app, Response)
        res = self._signup_helper()
        self.auth_headers = self._auth_headers_builder(res, {})

    def tearDown(self):
        self.app_context.pop()
        # self.collection.drop()

    def _auth_headers_builder(self, res, headers=None):
        if headers is None:
            headers = {}
        r_json = json.loads(res.get_data(as_text=True))
        headers['user_id'] = r_json.get('_id', '')
        headers['access_token'] = r_json.get('access_token', '')
        return headers

    def _signup_helper(self, email='khoa@consumit.com', password='P@ssw0rd'):
        return self.client.post(url_for('api_1_0.signup'),
                                json_data={'email': email, 'password': password})

    def _add_item_helper(self, item={}):
        if not item:
            item = {
                "type": "fruit",
                "name": "apple",
                "exp_days": 10,
                "quantity": 10,
                "quantity_type": "count"
            }
        res = self.client.post(url_for('api_1_0.add_get_item_by_userid'),
                               json_data=item,
                               headers=self.auth_headers)
        return res

    def _get_item_id_helpder(self, iid):
        res = self.client.get(url_for('api_1_0.get_del_item_by_id', item_id=iid),
                              headers=self.auth_headers)
        return res

    def _get_item_expiry(self, start, end, page, size, expiry='both'):
        query = {}
        query['end'] = end
        query['start'] = start
        query['page'] = page
        query['size'] = size
        query['expiry'] = expiry
        res = self.client.get(url_for('api_1_0.get_item_expiry', **query),
                              headers=self.auth_headers)
        return res

    def test_add_item_success(self):

        res = self._add_item_helper()
        assert res.status_code == 201, res

    def test_add_item_missing_field(self):
        res = self._add_item_helper(item={'name': 'apple'})
        assert res.status_code == 400, res

    def test_get_item_id(self):
        res = self._add_item_helper()
        r_json = json.loads(res.get_data(as_text=True))
        res = self._get_item_id_helpder(iid=r_json.get('_id'))
        assert res.status_code == 200, res

    def test_get_item_id_not_found(self):
        res = self._get_item_id_helpder(iid='5a0ec2d618091637ac0786ff')
        assert res.status_code == 404, res

    def test_get_items_time(self):
        today = datetime.now()
        tmr = today + timedelta(days=1)
        query = {'start': int(today.timestamp()),
                 'end': int(tmr.timestamp())}
        no = 3
        for _ in range(no):
            self._add_item_helper()
        res = self.client.get(url_for('api_1_0.add_get_item_by_userid', **query),
                              headers=self.auth_headers)
        assert res.status_code == 200, res
        data = res.get_data(as_text=True)
        assert len(json.loads(data)) == no, data

    def _update_item_quantity(self, iid, action='eat', amount=10, message='good'):
        upd = {'action': action,
               'amount': amount,
               'message': message}
        return self.client.put(url_for('api_1_0.update_item_quantity', item_id=iid),
                              json_data=upd,
                              headers=self.auth_headers)

    def test_update_item_quantity(self):
        res = self._add_item_helper()
        r_json = json.loads(res.get_data(as_text=True))
        iid = r_json.get('_id')
        res = self._update_item_quantity(iid=iid)
        assert res.status_code == 200, res
        r_json = json.loads(res.get_data(as_text=True))
        assert r_json['quantity'] == 0, r_json

    def test_get_item_expiry_pagination(self):
        num = 3
        for _ in range(num):
            self._add_item_helper()
        today = datetime.now().date()
        start = datetime(today.year, today.month, today.day)

        # check when size >= num of docs
        res = self._get_item_expiry(start=start.timestamp(), end=0, page=0, size=num)
        assert res.status_code == 200, res
        r_json = json.loads(res.get_data(as_text=True))
        assert len(r_json) == num, 'check when size >= num of docs'

        # check when size < num of docs
        res = self._get_item_expiry(start=start.timestamp(), end=0, page=0, size=1)
        assert res.status_code == 200, res
        r_json = json.loads(res.get_data(as_text=True))
        assert len(r_json) == 1, 'check when size < num of docs'

        # check when size = 0 to get everything
        res = self._get_item_expiry(start=start.timestamp(), end=0, page=0, size=0)
        assert res.status_code == 200, res
        r_json = json.loads(res.get_data(as_text=True))
        assert len(r_json) == num, 'check when size=0 to get all'

    def test_get_item_expiry_consumed_expired(self):
        num = 3
        for _ in range(num):
            self._add_item_helper()
        today = datetime.now().date()
        start = datetime(today.year, today.month, today.day)
        res = self._get_item_expiry(start=start.timestamp(), end=0, page=0, size=num)
        r_json = json.loads(res.get_data(as_text=True))
        # consumed 1
        iid = r_json[0]['_id']
        self._update_item_quantity(iid=iid)

        # check expired items
        res = self._get_item_expiry(start=start.timestamp(), end=0, page=0, size=num, expiry='expired')
        r_json = json.loads(res.get_data(as_text=True))
        assert len(r_json) == num - 1, 'check expired items'

        # check consumed items
        res = self._get_item_expiry(start=start.timestamp(), end=0, page=0, size=num, expiry='consumed')
        r_json = json.loads(res.get_data(as_text=True))
        assert len(r_json) == 1, 'check consumed items'

        # check both expired and consumed items
        res = self._get_item_expiry(start=start.timestamp(), end=0, page=0, size=num, expiry='both')
        r_json = json.loads(res.get_data(as_text=True))
        assert len(r_json) == num, 'check consumed and expired items'


