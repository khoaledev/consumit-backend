from app import create_app, mongo
from app.model.food_items import FoodItem
import os
from datetime import datetime

app = create_app(os.getenv('CONSUMIT_CONFIG') or 'default')


def update_expiration():
    with app.app_context():
        find = {FoodItem.IS_EXPIRED: False, FoodItem.IS_CONSUMED: False, FoodItem.EXP_DATE: {'$lte': datetime.utcnow()}}
        update = {'$set': {FoodItem.IS_EXPIRED: True}}
        cursor = mongo.db.food_items.update(find, update, multi=True)
        print(cursor)


update_expiration()
