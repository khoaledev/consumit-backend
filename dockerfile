#A simple Flask app container
FROM python:3.6
MAINTAINER khoale88 "lenguyenkhoa1988@gmail.com"

#Place app in container
COPY . /app
WORKDIR /app

RUN pip install -r requirements.txt

CMD python manage.py runserver
