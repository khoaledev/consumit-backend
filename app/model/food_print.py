"""
food print model
"""
from .mongo_model import MongoModel
from .. import mongo
from functools import reduce


class FoodPrint(MongoModel):
    """
    represent food print entry in analysis mongo collection

    - Support addition two FoodPrint.
    The result is a new FoodPrint item whose data is the merged data of datum
    in both FoodPrints' data. Attributes other than data is keep the same as
    the fist element in addition. Also leverage addition two FoodPrintDatum
    """
    USER_ID = 'user_id'
    GRANULARITY = 'granularity'
    TYPE = 'type'
    START_TIME = 'start_time'
    DATA = 'data'

    @classmethod
    def new_item(cls, uid, granularity, fp_type, start_time, data):
        return FoodPrint({
            FoodPrint.USER_ID: uid,
            FoodPrint.GRANULARITY: granularity,
            FoodPrint.TYPE: fp_type,
            FoodPrint.START_TIME: start_time,
            FoodPrint.DATA: data
        })

    @classmethod
    def _get_collection(cls):
        return mongo.db.analysis

    def compute_total(self):
        """
        compute/aggregate the total entry for this food print record

        - Currently a food print dictionary/object only has quantitative information
        of wasted, consumed, and total for each food name in data attribute.
        - This function is to compute the total within this record.
        - For example:
            a record has data of:
                peach: consumed: 3, wasted: 0, total 5;
                apple: consumed: 4, wasted, 1, total 10;
                banana: consumed: 10, wasted, 5, total 15;
            This function will append to that record a total FoodPrintDatum object in data
            with key = total, value = { consumed: 17 (3+4+10),
                                        wasted: 6(0+1+5),
                                        total: 30(5+10+15),
                                        name: 'total'}

        :return: FoodPrint object
        """
        data = getattr(self, FoodPrint.DATA, {})
        total = reduce(
            lambda tt, it: tt + it,
            data.values(),
            FoodPrintDatum.new_item(name='total')
        )
        data['total'] = total
        setattr(self, FoodPrint.DATA, data)
        return self

    def _add_data(self, other):
        """
        Merge two data. Leverage addition operator of two FoodPrintDatum

        food key/name is the union of two data sets
        for each key, compute the total of wasted, consumed, and total

        :param other: another FoodPrint
        :return: FoodPrint
        """
        my_data = getattr(self, FoodPrint.DATA, {})
        other_data = getattr(other, FoodPrint.DATA, {})
        keys = set().union(my_data.keys(), other_data.keys())
        data = {}
        for key in keys:
            datum = my_data.get(key, FoodPrintDatum.new_item(name=key))
            datum += other_data.get(key, FoodPrintDatum.new_item(name=key))
            data[key] = datum
        return data

    def __add__(self, other):
        """
        add two food print

        - All attributes except DATA is taken from the self or first element
        - DATA attributes is a merge of two DATA of two food print
            Contains all food name/key from both data of two food print records
            Each food print datum is the sum of two datum
            (sum of wasted, sum of consumed, sum of total)

        :param other: another FoodPrint
        :return: FoodPrint object
        """
        if not isinstance(other, FoodPrint):
            raise Exception('cannot add FoodPrint with {}'.format(type(other).__name__))
        data = self._add_data(other)
        return FoodPrint.new_item(
            uid=getattr(self, FoodPrint.USER_ID, ''),
            granularity=getattr(self, FoodPrint.GRANULARITY, ''),
            fp_type=getattr(self, FoodPrint.TYPE, ''),
            start_time=getattr(self, FoodPrint.START_TIME),
            data=data
        )

    def to_dict(self, include=(), exclude=(), deepcopy=False):
        """
        override to parse FoodPrintDatum

        :param include:
        :param exclude:
        :param deepcopy:
        :return:
        """
        d = super().to_dict(include, exclude, deepcopy=True)
        if self.DATA in d:
            d[self.DATA] = {key: datum.to_dict() for key, datum in d.get(self.DATA, {}).items()}
        return d

    def from_dict(self, doc, include=(), exclude=()):
        """
        override to parse FoodPrintDatum

        :param doc:
        :param include:
        :param exclude:
        :return:
        """
        super().from_dict(doc, include, exclude)
        if self.DATA not in exclude:
            setattr(self, FoodPrint.DATA,
                    {key: FoodPrintDatum(value) for key, value in getattr(self, FoodPrint.DATA, {}).items()})

    def __init__(self, interable):
        """
        compute total for this record data

        :param interable:
        """
        super().__init__(interable)
        data = getattr(self, FoodPrint.DATA, {})
        if not data.get('total', None):
            self.compute_total()


class FoodPrintDatum(MongoModel):
    """
    Represent an item of data in FoodPrint's data attribute

    - Support addition operator
        When two Datum is added, a new set of total, consumed, and wasted are computed
        with the value is the sum of corresponding values in the two Data
        Name and type are the same as the first operator
    """
    NAME = 'name'
    TOTAL = 'total'
    CONSUMED = 'consumed'
    WASTED = 'wasted'
    TYPE = 'type'

    @classmethod
    def new_item_json(cls, name, wasted=0, consumed=0, total=0, quantity_type=''):
        return {
            FoodPrintDatum.NAME: name,
            FoodPrintDatum.TOTAL: total,
            FoodPrintDatum.CONSUMED: consumed,
            FoodPrintDatum.WASTED: wasted,
            FoodPrintDatum.TYPE: quantity_type}

    @classmethod
    def new_item(cls, name, wasted=0, consumed=0, total=0, quantity_type=''):
        return FoodPrintDatum(cls.new_item_json(name, wasted, consumed, total, quantity_type))

    def __add__(self, other):
        """
        add two FoodPrintDatum, name and type will inherit from the 1st element
        consumed, wasted, total are the sum of corresponding attributes of two data

        :param other: another FoodPrintDatum
        :return: new FoodPrintDatum
        """
        if not isinstance(other, FoodPrintDatum):
            raise Exception('cannot add FoodPrintDatum with {}'.format(type(other).__name__))

        return FoodPrintDatum.new_item(
            name=getattr(self, FoodPrintDatum.NAME),
            wasted=getattr(self, FoodPrintDatum.WASTED, 0) + getattr(other, FoodPrintDatum.WASTED, 0),
            consumed=getattr(self, FoodPrintDatum.CONSUMED, 0) + getattr(other, FoodPrintDatum.CONSUMED, 0),
            total=getattr(self, FoodPrintDatum.TOTAL, 0) + getattr(other, FoodPrintDatum.TOTAL, 0),
            quantity_type=getattr(self, FoodPrintDatum.TYPE)
        )
