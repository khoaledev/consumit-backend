import boto3


class ConsumitPushNotification(object):
    """
    provide method to send different push notification
    """
    def __init__(self, app=None):
        self.config = {}
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        """
        save relevant configuration
        :param app: app with configuration
        :return:
        """
        if app.config['TESTING']:
            return
        kwargs = {
            'resource': 'pinpoint',
            'secret_key': app.config['AWS_PINPOINT_SECRET_KEY'],
            'access_id': app.config['AWS_PINPOINT_ACCESS_ID'],
            'region_name': app.config['AWS_PINPOINT_REGION_NAME'],
            'application_id': app.config['AWS_PINPOINT_APPLICATION_ID'],
            'testing': app.config['TESTING'] or False
        }
        self.config = kwargs

    def _send_push_message(self, message_request):
        cf = self.config
        if cf['testing']:
            return

        client = boto3.client(
            'pinpoint',
            aws_access_key_id=cf['access_id'],
            aws_secret_access_key=cf['secret_key'],
            region_name=cf['region_name'])

        res = client.send_messages(
            ApplicationId=cf['application_id'],
            MessageRequest=message_request
        )
        print(res)

    def send_food_expiration_message(self, tokens, title, body, channel_type='GCM', *args):
        """
        send push notification to inform users how many food is going to expired in
        a couple of days

        Only support android right now and no validation for channel_type

        :param tokens:
        :param title:
        :param body:
        :param channel_type:
        :param args:
        :return:
        """
        addresses = {}
        for token in tokens:
            addresses[token] = {
                'TitleOverride': title or 'Food Expiration',
                'BodyOverride': body or 'Food Expiration',
                'ChannelType': channel_type or 'GCM'
            }

        gmc_msg = {
            'Action': 'OPEN_APP',
            'Body': 'Food Expiration',
            'CollapseKey': 'Food Expiration',
            'Title': 'Food Expiration',
        }
        message_request = {'Addresses': addresses,
                           'MessageConfiguration': {
                               'GCMMessage': gmc_msg
                           }}
        self._send_push_message(message_request)
