import unittest
from flask import url_for, json, Response
from app import create_app, mongo
from testing import ConSumitTestClient


class UsersTestCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        self.collection = mongo.db.users
        self.collection.remove()
        # use ConsumitTestClient instead of FlaskClient to add default content_type = 'application/json' and auto json dumps
        # self.client = self.app.test_client()
        self.client = ConSumitTestClient(self.app, Response)

    def tearDown(self):
        self.app_context.pop()
        # self.collection.drop()

    def _auth_headers_builder(self, res, headers=None):
        if headers is None:
            headers = {}
        r_json = json.loads(res.get_data(as_text=True))
        headers['user_id'] = r_json.get('_id', '')
        headers['access_token'] = r_json.get('access_token', '')
        return headers

    def _signup_helper(self, email='khoa@consumit.com', password='P@ssw0rd'):
        return self.client.post(url_for('api_1_0.signup'),
                                json_data={'email': email, 'password': password})

    def _signin_helper(self, email='khoa@consumit.com', password='P@ssw0rd'):
        return self.client.post(url_for('api_1_0.signin'),
                                json_data={'email': email, 'password': password})

    def _signout_helper(self, user_id, access_token):
        return self.client.post(url_for('api_1_0.signout'),
                                headers={'user_id': user_id, 'access_token': access_token})

    def _get_profile_helper(self, user_id, access_token):
        return self.client.get(url_for('api_1_0.get_profile', user_id=user_id),
                               headers={'user_id': user_id, 'access_token': access_token})

    def _update_profile_helper(self, profile, user_id, access_token):
        return self.client.put(url_for('api_1_0.update_profile', user_id=user_id),
                               headers={'user_id': user_id, 'access_token': access_token},
                               json_data=profile)

    def _gen_alexa_code_helper(self, user_id, access_token):
        return self.client.get(url_for('api_1_0.get_alexa_code', user_id=user_id),
                               headers={'user_id': user_id, 'access_token': access_token})

    def _register_alexa_helper(self, alexa_id, alexa_code):
        return self.client.put(url_for('api_1_0.register_alexa'),
                               json_data={'alexa_code': alexa_code, 'alexa_id': alexa_id})

    def _remove_alexa_helper(self, user_id, access_token):
        return self.client.put(url_for('api_1_0.remove_alexa', user_id=user_id),
                               headers={'user_id': user_id, 'access_token': access_token})

    def _get_user_by_alexa_id_helper(self, alexa_id):
        return self.client.get(url_for('api_1_0.get_user_by_alexa_id', alexa_id=alexa_id))

    def test_signup_success(self):
        response = self._signup_helper()
        assert response.status_code == 201, response

    def test_signup_conflict(self):
        self._signup_helper()
        response = self._signup_helper()
        assert response.status_code == 409, response

    def test_signup_bad_pass(self):
        response = self._signup_helper(password='p')
        assert response.status_code == 400, response

    def test_signin_success(self):
        self._signup_helper()
        response = self._signin_helper()
        assert response.status_code == 200, response

    def test_signin_twice_success(self):
        self._signup_helper()
        self._signin_helper()
        response = self._signin_helper()
        assert response.status_code == 200, response

    def test_signin_notfound(self):
        # no registration
        response = self._signin_helper()
        assert response.status_code == 404, response

    def test_signout_success(self):
        response = self._signup_helper()
        response = self._signout_helper(**self._auth_headers_builder(response))
        assert response.status_code == 204, response

    def test_signout_twice_success(self):
        response = self._signup_helper()
        headers = self._auth_headers_builder(response)
        self._signout_helper(**headers)
        response = self._signout_helper(**headers)
        assert response.status_code == 204, response
        # assert response.status_code == 401, response

    def test_get_profile_success(self):
        response = self._signup_helper()
        response = self._get_profile_helper(**self._auth_headers_builder(response))
        assert response.status_code == 200, response

    def test_update_profile_success(self):
        response = self._signup_helper()
        headers = self._auth_headers_builder(response)
        response = self._get_profile_helper(**headers)
        r_json = json.loads(response.get_data(as_text=True))
        r_json['gender'] = 'male'
        response = self._update_profile_helper(r_json, **headers)
        assert response.status_code == 200, response

    def test_alexa_code_generation(self):
        response = self._signup_helper()
        headers = self._auth_headers_builder(response)
        response = self._gen_alexa_code_helper(**headers)
        assert response.status_code == 200, response
        r_json = json.loads(response.get_data(as_text=True))
        assert r_json['alexa_code'] != '', r_json

    def test_alexa_registration_success(self):
        response = self._signup_helper()
        headers = self._auth_headers_builder(response)
        response = self._gen_alexa_code_helper(**headers)
        r_json = json.loads(response.get_data(as_text=True))
        alexa_code = r_json['alexa_code']['code']
        alexa_id = 'tesing alexa id'
        response = self._register_alexa_helper(alexa_id=alexa_id, alexa_code=alexa_code)
        assert response.status_code == 200, response
        headers = self._auth_headers_builder(response)
        response = self._get_profile_helper(**headers)
        r_json = json.loads(response.get_data(as_text=True))
        assert r_json['alexa_id'] == alexa_id, r_json

        response = self._register_alexa_helper(alexa_id=alexa_id, alexa_code=alexa_code)
        assert response.status_code == 200, 'register same device twice {}'.format(response)

    def test_alexa_registration_invalid_code(self):
        response = self._signup_helper()
        headers = self._auth_headers_builder(response)
        self._gen_alexa_code_helper(**headers)
        alexa_code = 132434654567
        alexa_id = 'tesing alexa id'
        response = self._register_alexa_helper(alexa_id=alexa_id, alexa_code=alexa_code)
        assert response.status_code == 404, 'invalid code'

    def test_alexa_registration_duplication_id(self):
        response = self._signup_helper()
        headers = self._auth_headers_builder(response)
        response = self._gen_alexa_code_helper(**headers)
        r_json = json.loads(response.get_data(as_text=True))
        alexa_code = r_json['alexa_code']['code']
        alexa_id = 'tesing alexa id'
        self._register_alexa_helper(alexa_id=alexa_id, alexa_code=alexa_code)

        response = self._signup_helper(email='khoa2@consumit.com')
        headers = self._auth_headers_builder(response)
        response = self._gen_alexa_code_helper(**headers)
        r_json = json.loads(response.get_data(as_text=True))
        alexa_code = r_json['alexa_code']['code']
        response = self._register_alexa_helper(alexa_id=alexa_id, alexa_code=alexa_code)
        assert response.status_code == 409, response

    def test_alexa_registration_two_device_now_allowed(self):
        response = self._signup_helper()
        headers = self._auth_headers_builder(response)
        response = self._gen_alexa_code_helper(**headers)
        r_json = json.loads(response.get_data(as_text=True))
        alexa_code = r_json['alexa_code']['code']
        alexa_id = 'tesing alexa id'
        self._register_alexa_helper(alexa_id=alexa_id, alexa_code=alexa_code)
        alexa_id = 'device 2'
        response = self._register_alexa_helper(alexa_id=alexa_id, alexa_code=alexa_code)
        assert response.status_code == 403, response

    def test_remove_alexa(self):
        response = self._signup_helper()
        headers = self._auth_headers_builder(response)
        response = self._remove_alexa_helper(**headers)
        assert response.status_code == 204, response

    def test_get_user_by_alexa_id(self):
        response = self._signup_helper()
        headers = self._auth_headers_builder(response)
        user_id = headers['user_id']
        response = self._gen_alexa_code_helper(**headers)
        r_json = json.loads(response.get_data(as_text=True))
        alexa_code = r_json['alexa_code']['code']
        alexa_id = 'tesing alexa id'
        self._register_alexa_helper(alexa_id=alexa_id, alexa_code=alexa_code)
        response = self._get_user_by_alexa_id_helper(alexa_id=alexa_id)
        r_json = json.loads(response.get_data(as_text=True))
        assert r_json['_id'] == user_id, r_json