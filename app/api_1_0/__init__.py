from flask import Blueprint

api_1_0_blueprint = Blueprint('api_1_0', __name__, static_folder='../static/')

from . import users, food_items, analysis, recommendation
