from flask import Blueprint

index_blueprint = Blueprint('index', __name__, static_folder='../static/')

from . import index
