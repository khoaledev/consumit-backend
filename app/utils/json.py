from flask import json
from bson import ObjectId
from ..model.mongo_model import MongoModel


class MongoEncoder(json.JSONEncoder):
    """
    Custom JSON Encoder to serialize Mongo Models and ObjectId
    """
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        elif isinstance(o, MongoModel):
            return o.to_dict()
        return json.JSONEncoder.default(self, o)
