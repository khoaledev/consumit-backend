import functools
from flask import request
from ..model.user import User
from werkzeug.exceptions import BadRequest, Unauthorized


def verify_user_token():
    """
    decorator for flask request
    check if headers contain user_id and access_token fields, else 400
    check if user_id is valid in db, else 404
    check if token is belong to user, else 403
    """
    def verify(f):
        @functools.wraps(f)
        def decorated_function(*args, **kwds):
            nokeys = [k for k in ['user_id', 'access_token'] if k not in request.headers]
            if nokeys:
                raise BadRequest(','.join(nokeys))
            user_id = request.headers.get('user_id', '')
            access_token = request.headers.get('access_token', '')
            if not User.find(user_id).verify_token(access_token):
                raise Unauthorized('invalid token for %s' % (user_id))
            return f(*args, **kwds)
        return decorated_function
    return verify


def verify_secret_handshake():
    """
    decorator for flask request
    check if headers contain hand_shake fields, else 400
    check if user_id is valid, else 403
    """
    def verify(f):
        @functools.wraps(f)
        def decorated_function(*args, **kwds):
            nokeys = [k for k in ['hand_shake'] if k not in request.headers]
            if nokeys:
                raise BadRequest(','.join(nokeys))
            secret_code = request.headers.get('hand_shake', '')
            if secret_code != 'OnlyIfYouCanGuest2018#':
                raise Unauthorized('invalid handshake code')
            return f(*args, **kwds)
        return decorated_function
    return verify
