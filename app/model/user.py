"""
User model
"""
from datetime import datetime, timedelta
from bson import ObjectId
from .mongo_model import MongoModel
from pymongo.collection import ReturnDocument
from ..utils import pwd_utils, numbers
from flask import abort
from pymongo.errors import DuplicateKeyError
from .. import mongo
from ..exceptions import ResourceNotFound
import pytz


class User(MongoModel):
    """
    represent a user in user mongo collection
    """
    HASHED_PASS = 'hashed_pass'
    ACCESS_TOKEN = 'access_token'
    EMAIL = 'email'
    MEMBER_SINCE = 'member_since'
    PROFILE_PIC = 'profile_pic'
    DISPLAY_NAME = 'display_name'
    DOB = 'DOB'
    GENDER = 'gender'
    LOCATION = 'location'
    ALEXA_CODE = 'alexa_code'
    ALEXA_ID = 'alexa_id'
    ANALYSIS = 'analysis'
    PUSH_NOTI_TOKEN = 'push_noti_token'

    __PROF_FIELDS = (EMAIL, MEMBER_SINCE, PROFILE_PIC, DISPLAY_NAME, DOB, GENDER, LOCATION, ALEXA_ID)

    @classmethod
    def get_all_userid(cls):
        """
        get all user id in user collection
        :return: generator of user id string
        """
        col = cls._get_collection()
        cursor = col.find({}, {cls.MG_ID: 1})
        for user in cursor:
            yield str(user['_id'])

    @classmethod
    def get_all_user_push_tokens(cls):
        """
        get all push_token in user collection
        :return: User object
        """
        col = cls._get_collection()
        # get only those have GCM tokens
        cursor = col.find({'{}.{}'.format(cls.PUSH_NOTI_TOKEN, 'GCM'): {'$gt': []}},
                          {cls.PUSH_NOTI_TOKEN: 1})
        for user in cursor:
            yield User(user)

    @classmethod
    def get_user_push_token(cls, user_id):
        """
        get push token of a user
        :param user_id:
        :return:
        """
        col = cls._get_collection()
        # get only those have GCM tokens
        user = col.find_one({User.MG_ID: ObjectId(user_id) ,'{}.{}'.format(cls.PUSH_NOTI_TOKEN, 'GCM'): {'$gt': []}},
                            {cls.PUSH_NOTI_TOKEN: 1})
        return User(user)

    @classmethod
    def _get_collection(cls):
        """
        get the user collection
        :return: users collection
        """
        return mongo.db.users

    @classmethod
    def find_by_email(cls, email):
        """
        Find a user by email
        :param email: email of user
        :return: User object
        """
        result = mongo.db.users.find_one({User.EMAIL: email.lower()})
        if not result:
            raise ResourceNotFound(resource=cls.__name__, message=email)
        return User(result)

    @classmethod
    def _get_prof_fields(cls):
        """
        helper function to get profile fields
        :return:
        """
        return cls.__PROF_FIELDS

    @classmethod
    def new_user(cls, email, password):
        """
        create a new user with email and password
        :param email:
        :param password:
        :return: a User Object
        """
        return User(cls._new_user_doc(email, password))

    @classmethod
    def _new_user_doc(cls, email, password):
        """
        initialize a new user
        :param email:
        :param password:
        :return: a dictionary with preset attribute for user
        """
        pwd_utils.validate_password(password)
        usr = {cls.EMAIL: email.lower(),
               cls.HASHED_PASS: pwd_utils.generate_pass_hash(password),
               cls.MEMBER_SINCE: datetime.utcnow(),
               cls.PROFILE_PIC: 'pic_1',
               cls.GENDER: 'unknown',
               cls.DOB: '',
               cls.LOCATION: '',
               cls.ACCESS_TOKEN: '',
               cls.DISPLAY_NAME: ''}
        return usr

    def signup(self):
        """
        Current user sign up. This should be called after new_user method
        :return: a User object
        """
        doc = self.to_dict()
        try:
            mongo.db.users.insert_one(doc)
            setattr(self, User.MG_ID, doc.get(User.MG_ID))
            return self
        except DuplicateKeyError:
            abort(409, doc.get(User.EMAIL))

    def signin(self):
        """
        Current user sign in. Token will be create if there is no token
        :return: current user with token
        """
        self._create_token()
        return self

    def _create_token(self):
        """
        helper function to create token
        :return:
        """
        access_token = getattr(self, User.ACCESS_TOKEN, '')
        if not access_token or access_token is None:
            access_token = pwd_utils.generate_random_hex()
            setattr(self, User.ACCESS_TOKEN, access_token)
            self.save(User.ACCESS_TOKEN)

    def signout(self):
        """
        Current user log out
        :return:
        """
        # not remove now
        # self._remove_token()
        return self

    def _remove_token(self):
        """
        helper function to remove token
        :return:
        """
        setattr(self, User.ACCESS_TOKEN, '')
        self.save(User.ACCESS_TOKEN)

    def verify_pwd(self, pwd):
        """
        check if the password match with current password
        :param pwd:
        :return:
        """
        hashed_pass = getattr(self, User.HASHED_PASS)
        pwd_utils.check_pass_hash(hashed_pass, pwd)
        return self

    def get_token(self):
        """
        read token
        :return: a dictionary include token and id fields
        """
        return {User.MG_ID: str(getattr(self, User.MG_ID)),
                User.ACCESS_TOKEN: getattr(self, User.ACCESS_TOKEN)}

    def verify_token(self, tkn):
        """
        check if the token matches with current token
        :param tkn:
        :return:
        """
        return getattr(self, User.ACCESS_TOKEN, '') == tkn

    def get_profile(self):
        """
        read user profile
        :return: a dictionary of user profile
        """
        fields = User._get_prof_fields()
        return self.to_dict(include=fields)

    def update_profile(self, prof_doc):
        """
        update user profile
        :param prof_doc: new dictionary of user profile
        :return: a User object with updated profile
        """
        self._validate_profile(prof_doc)
        fields = User._get_prof_fields()
        self.from_dict(prof_doc, fields)
        self.save(*fields)
        return self

    def _validate_profile(self, profile):
        # TODO: Validate profile
        return self

    def save(self, *fields):
        """
        save current user in db
        :param fields: saving fields, if fields is empty, the whole document is saved
        :return: update result
        """
        doc = self.to_dict(include=fields, exclude=[User.MG_ID, User.EMAIL])
        uid = getattr(self, User.MG_ID, None)
        return mongo.db.users.update_one(
            {User.MG_ID: uid},
            {'$set': doc})

    @classmethod
    def find_by_alexa_id(cls, alexa_id):
        """

        :param alexa_id:
        :return:
        """
        doc = mongo.db.users.find_one({User.ALEXA_ID: alexa_id})
        if not doc:
            raise ResourceNotFound(User.__class__.__name__, 'alexa id not found')
        return User(doc)

    def generate_alexa_code(self):
        """
        generate alexa code to register an alexa device
        the code is valid for 5 minutes.
        If the request run before 5 minutes of expiration
        the same code is return. Otherwise, new code is created and return.
        The user has to have no registered alexa id,
        or else 403 Unauthorized code is return
        :return:
        """
        alexa_id = getattr(self, User.ALEXA_ID, '')
        alexa_code = getattr(self, User.ALEXA_CODE, {})
        if alexa_id:
            abort(403)
        utcnow = datetime.utcnow().replace(tzinfo=pytz.timezone('utc'))
        if not alexa_code or utcnow > alexa_code['exp']:
            alexa_code = {
                'code': numbers.randint_n_digits(),
                'exp': utcnow + timedelta(minutes=5)}
            setattr(self, User.ALEXA_CODE, alexa_code)
            self.save(User.ALEXA_CODE)
        return self

    @classmethod
    def register_alexa(cls, alexa_code, alexa_id):
        """
        register an alexa id. The valid generated code is required.
        the code is invalid if it is not exist in db or the time has passed 5 minutes,
        else 404 Resource Not Found - Invalid code is return

        It is required that the user does not have an alexa id registered,
        else 403 Unauthorized - already registered with another alexa is return

        :param alexa_code:
        :param alexa_id:
        :return: User object
        """
        # check if there is a valid code that is within the last 5 minutes
        query = [{'{}.{}'.format(User.ALEXA_CODE, 'code'): alexa_code},
                 {'{}.{}'.format(User.ALEXA_CODE, 'exp'): {'$gte': datetime.utcnow()}}]
        query = {'$and': query}
        result = mongo.db.users.find_one(query)
        if not result:
            raise ResourceNotFound(User.__class__.__name__, 'invalid code')
        user = User(result)

        if getattr(user, cls.ALEXA_ID, '') == alexa_id:
            # if register for the same device, just return
            return user
        elif getattr(user, cls.ALEXA_ID, ''):
            # if register the second device, not allowed
            abort(403, 'already registered with another alexa')

        try:
            # check if another user has this alexa
            user2 = cls.find_by_alexa_id(alexa_id)
            abort(409, 'already registered in account with email {}'.format(getattr(user2, cls.EMAIL)))
        except ResourceNotFound:
            pass

        # finally register this alexa
        setattr(user, User.ALEXA_ID, alexa_id)
        user.save(User.ALEXA_ID)
        return user

    @classmethod
    def remove_alexa(cls, user_id):
        """
        remove alexa id of a user
        :param user_id:
        :return:
        """
        mongo.db.users.update_one({User.MG_ID: ObjectId(user_id)},
                                  {'$set': {User.ALEXA_ID: ''}})

    def get_analysis(self):
        return self.to_dict(include=[User.ANALYSIS])

    def get_suggestive_shopping_list(self):
        """
        retrieve suggestive shopping list
        :return: an array of food item name as a suggestive shopping list
        """
        analysis = self.get_analysis()
        shopping_list = analysis.get('analysis', {}).get('suggestive_shopping_list', [])
        return [item.title() for item in shopping_list]

    @classmethod
    def validate_push_token(cls, token, os):
        """
        validate push notification token.
        token has not to be empty
        os has not to be empty
        current supported os is "android"

        if either of the above condition is unmet, 404 error is return
        with corresponding message

        :param token:
        :param os:
        :return:
        """
        if not token:
            abort(400, 'token missing')
        if not os:
            abort(400, 'os missing')
        if os not in ['android']:
            abort(400, 'not support {} operating system'.format(os))
        return cls

    @classmethod
    def register_push_token(cls, user_id, token, os):
        """
        register a push notification and retain only the last 5 tokens for an os
        :param user_id:
        :param token:
        :param os: operating system
        :return:
        """

        # cls.validate_push_token(token, os)

        key = ''
        if os.lower() == 'android':
            key = 'GCM'

        user = User.find(user_id)
        push_tokens = getattr(user, User.PUSH_NOTI_TOKEN, {})
        tokens = push_tokens.get(key, [])
        if token in tokens:
            return push_tokens
        else:
            tokens.append(token)
            tokens = tokens[-5:]
            push_tokens[key] = tokens
            setattr(user, User.PUSH_NOTI_TOKEN, push_tokens)
            user.save(User.PUSH_NOTI_TOKEN)
        return push_tokens

    @classmethod
    def remove_push_token(cls, user_id, token, os):
        """
        remove a push notification token
        :param user_id:
        :param token:
        :param os: operating system
        :return:
        """
        key = ''
        if os.lower() == 'android':
            key = 'GCM'
        if key:
            update_query = {
                '$pull': {
                    '{}.{}'.format(cls.PUSH_NOTI_TOKEN, key): token
                }
            }
        db = cls._get_collection()
        db.find_one_and_update(
            {cls.MG_ID: ObjectId(user_id)},
            update_query,
            upsert=False)
