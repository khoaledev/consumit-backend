from flask import Flask
from flask_pymongo import PyMongo, ASCENDING, DESCENDING
from config import CONFIG
from .utils.json import MongoEncoder
from .utils.sqs import ConsumitSQS
from .utils.aws_pinpoint import ConsumitPushNotification

mongo = PyMongo()
sqs = ConsumitSQS()
pinpoint = ConsumitPushNotification()


def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(CONFIG[config_name])
    app.json_encoder = MongoEncoder
    CONFIG[config_name].init_app(app)

    mongo.init_app(app)
    sqs.init_app(app)
    pinpoint.init_app(app)

    with app.app_context():
        mongo.db.users.ensure_index('email', unique=True)
        mongo.db.food_items.ensure_index([('user_id', ASCENDING), ('input_date', DESCENDING)])

    from .api_1_0 import api_1_0_blueprint
    app.register_blueprint(api_1_0_blueprint, url_prefix='/api/v1.0')
    from .index import index_blueprint
    app.register_blueprint(index_blueprint, url_prefix='')

    return app


from . import error_handlers
