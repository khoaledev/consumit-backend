from flask.testing import FlaskClient
from flask import json

class ConSumitTestClient(FlaskClient):
    
    def open(self, *args, **kwargs):
        kwargs['content_type'] = 'application/json'
        json_data = kwargs.pop('json_data', None)
        if json_data:
            kwargs['data'] = json.dumps(json_data)
        return super(ConSumitTestClient, self).open(*args, **kwargs)

class Utils:
    pass