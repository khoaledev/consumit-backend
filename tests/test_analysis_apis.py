import unittest
from flask import current_app, url_for, json, Response
from app import create_app, mongo
from testing import ConSumitTestClient
from datetime import datetime, timedelta


class FoodItemsTestCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        self.collection = mongo.db.food_items
        self.collection.remove()
        mongo.db.users.remove()
        self.client = ConSumitTestClient(self.app, Response)
        res = self._signup_helper()
        self.auth_headers = self._auth_headers_builder(res, {})

    def tearDown(self):
        self.app_context.pop()
        # self.collection.drop()

    def _auth_headers_builder(self, res, headers=None):
        if headers is None:
            headers = {}
        r_json = json.loads(res.get_data(as_text=True))
        headers['user_id'] = r_json.get('_id', '')
        headers['access_token'] = r_json.get('access_token', '')
        return headers

    def _signup_helper(self, email='khoa@consumit.com', password='P@ssw0rd'):
        return self.client.post(url_for('api_1_0.signup'),
                                json_data={'email': email, 'password': password})

    def _get_most_wasted_consumed_analysis_helper(self):
        return self.client.get(url_for('api_1_0.get_most_wasted_consumed_analysis'),
                               headers=self.auth_headers)

    def test_get_most_wasted_analysis(self):
        res = self._get_most_wasted_consumed_analysis_helper()
        assert res.status_code == 200, res
