from .. import pinpoint as push
from .user import User
from .food_items import FoodItem
from ..utils.time import get_day_start
from datetime import timedelta


class PushNotification(object):
    """
    provide public interface to collect data and send notification
    """

    CHANNEL_GCM = 'GCM'

    @classmethod
    def send_food_expiration(cls, user_id=''):
        """
        collect info and send food expiration notification to a user or all users.

        - If user_id is given, send push message to that user only, else send to all users.
        - Collect android tokens belongs to each user.
        - If there is at least one token, count the number of expiring food within 2 days
        - Send message to that user and repeat the process as continuing to the next user

        :param user_id:
        :return:
        """
        days = 2
        today_start = get_day_start()
        end = today_start + timedelta(days=days)
        if user_id:
            users = [User.get_user_push_token(user_id)]
        else:
            users = User.get_all_user_push_tokens()

        for user in users:
            tokens = getattr(user, User.PUSH_NOTI_TOKEN, {})
            tokens = tokens.get(cls.CHANNEL_GCM, [])

            if not tokens:
                continue

            user_id = getattr(user, User.MG_ID)
            food_items = FoodItem.find_item_expiry(user_id, start=today_start.timestamp(), end=end.timestamp(),
                                                   expiry=FoodItem.EXPIRY_EXPIRED)

            if not len(food_items):
                return

            for item in food_items:

                print('days', getattr(item, FoodItem.DAYS_LEFT, ''))
                print('hours', getattr(item, FoodItem.HOURS_LEFT, ''))

            push.send_food_expiration_message(
                tokens=tokens,
                title='',
                body='{} foods expiring in {} days, please take action'.format(len(food_items), days))
