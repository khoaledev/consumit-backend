from flask import request, jsonify, abort
from . import api_1_0_blueprint as api
from ..model.food_items import FoodItem
from ..utils import auth_utils
from .. import sqs


@api.route('/food-items', methods=['POST', 'GET'])
@auth_utils.verify_user_token()
def add_get_item_by_userid():
    """
    create a food item OR read food items given a time range for input dates

    POST:
        need body fields: [name, type, quantity, quantity_type, exp_days, message]
        create, validate, and insert item

        send message to message queue for analysis

    GET:
        accept optional query params: [start, end, page, size, expiry, name, and cat]
        start and end are timestamp for input date
        page and size for pagination
            enabling pagination only if size is given
        expiry: expired | consumed | both
            any different value is treated as both
        name and cat for filtering

    :return: a food item or a list of food items
    """
    uid = request.headers.get('user_id', '')
    if request.method == 'POST':
        # validate input and insert item.
        req_json = request.get_json()
        item = FoodItem \
            .new_item(uid,
                      name=req_json.get('name', '').lower(),
                      food_type=req_json.get('type', ''),
                      quantity=req_json.get('quantity', 0),
                      qnt_type=req_json.get('quantity_type', ''),
                      exp_days=req_json.get('exp_days', 0),
                      message=req_json.get('message', '')) \
            .validate_and_create()

        # produce and send added_food message to the queue
        sqs.send_added_consumed_food(
            user_id=getattr(item, FoodItem.USER_ID),
            food_name=getattr(item, FoodItem.NAME),
            quantity=getattr(item, FoodItem.QUANTITY),
            food_type=getattr(item, FoodItem.TYPE),
            message='added_food',
            timestamp=getattr(item, FoodItem.LAST_UPDATE).timestamp()
        )

        # return success
        return jsonify(item), 201
    elif request.method == 'GET':
        """
        get item by user id and input date
        """
        start = float(request.args.get('start', 0))
        end = float(request.args.get('end', 0))
        page = int(request.args.get('page', 0))
        size = int(request.args.get('size', 0))
        expiry = request.args.get('expiry', FoodItem.EXPIRY_BOTH)
        name = request.args.get('name', '')
        cat = request.args.get('cat', '')

        items = FoodItem.find_by_user_id(uid, start, end, page, size, expiry, name, cat)
        return jsonify(items)


@api.route('/food-items/expiry', methods=['GET'])
@auth_utils.verify_user_token()
def get_item_expiry():
    """
    get food items given a time range for expiration dates

    accept optional query params: [start, end, page, size, expiry, name, and cat]
    start and end are timestamp for input date
    page and size for pagination
        enabling pagination only if size is given
    expiry: expired | consumed | both
        any different value is treated as both
    name and cat for filtering

    :return: a list of food items
    """
    uid = request.headers.get('user_id', '')
    start = float(request.args.get('start'))
    end = float(request.args.get('end', 0))
    page = int(request.args.get('page', 0))
    size = int(request.args.get('size', 0))
    expiry = request.args.get('expiry', FoodItem.EXPIRY_BOTH)
    name = request.args.get('name', '')
    cat = request.args.get('cat', '')
    if expiry not in [FoodItem.EXPIRY_EXPIRED, FoodItem.EXPIRY_CONSUMED, FoodItem.EXPIRY_BOTH]:
        expiry = FoodItem.EXPIRY_BOTH
    items = FoodItem.find_item_expiry(uid, start, end, page, size, expiry, name, cat)
    return jsonify(items)


@api.route('/food-items/<item_id>', methods=['GET', 'DELETE'])
@auth_utils.verify_user_token()
def get_del_item_by_id(item_id):
    """
    delete item given item id
    :param item_id:
    :return: 204
    """

    # TODO: send message to backend to update analysis
    if request.method == 'GET':
        item = FoodItem.find(item_id)
        return jsonify(item)
    elif request.method == 'DELETE':
        FoodItem.remove(item_id)
        return '', 204


@api.route('/food-items/<item_id>/quantity', methods=['PUT'])
@auth_utils.verify_user_token()
def update_item_quantity(item_id):
    """
    update food quantity or food consumption.
    accept body fields: [amount, action, message]
        message field is option

    send message to message queue for analysis

    :param item_id:
    :return: food item after updated
    """
    # get food item by food item id
    req_json = request.get_json()
    amount = float(req_json.get('amount', 0))
    item = FoodItem.find(item_id)

    # update item
    item.update_quantity(amount=amount,
                         action=req_json.get('action', 'eat'),
                         message=req_json.get('message', ''))

    # send a message to queue to update analysis
    sqs.send_added_consumed_food(
        user_id=getattr(item, FoodItem.USER_ID),
        food_name=getattr(item, FoodItem.NAME),
        quantity=amount,
        food_type=getattr(item, FoodItem.TYPE),
        message='consumed_food',
        timestamp=getattr(item, FoodItem.LAST_UPDATE).timestamp()
    )

    # return success
    return jsonify(item), 200
