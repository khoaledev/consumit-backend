import random


def randint_n_digits(num_digits=6):
    """
    generate a random integer with specified number of digit
    :param num_digits:
    :return:
    """
    range_start = 10 ** (num_digits - 1)
    range_end = (10 ** num_digits) - 1
    return random.randint(range_start, range_end)
