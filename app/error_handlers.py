from flask import jsonify
from .api_1_0 import api_1_0_blueprint as api
from . import exceptions


@api.app_errorhandler(exceptions.ResourceNotFound)
def handle_resourcenotfound(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    response.error_type = error.__class__.__name__
    return response
